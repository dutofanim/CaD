# gera o executável e as matrizes 10x10

gcc geraMatriz.c
mv a.out geraMatriz

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# gera o executável e multiplica as matrizes
# 10x10 com 2, 4, 6 e 8 threads

gcc matrizPthread.c -lpthread -lm lerArquivos.c 
mv a.out matrizPthread

./matrizPthread 2 10 10 10a 10 10 10b
./matrizPthread 4 10 10 10a 10 10 10b
./matrizPthread 6 10 10 10a 10 10 10b
./matrizPthread 8 10 10 10a 10 10 10b

##########################################################

# gera as matrizes 100x100

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# multiplica as matrizes 100x100 com 2, 4, 6 e 8 threads

./matrizPthread 2 100 100 100a 100 100 100b
./matrizPthread 4 100 100 100a 100 100 100b
./matrizPthread 6 100 100 100a 100 100 100b
./matrizPthread 8 100 100 100a 100 100 100b

##########################################################

# gera as matrizes 1000x1000

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# multiplica as matrizes 1000x1000 com 2, 4, 6 e 8 threads

./matrizPthread 2 1000 1000 1000a 1000 1000 1000b
./matrizPthread 4 1000 1000 1000a 1000 1000 1000b
./matrizPthread 6 1000 1000 1000a 1000 1000 1000b
./matrizPthread 8 1000 1000 1000a 1000 1000 1000b

##########################################################

