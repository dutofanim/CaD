# gera o executável e as matrizes 10x10

gcc geraMatriz.c
mv a.out geraMatriz

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# gera o executável e multiplica as matrizes 10x10


gcc matrizSerial.c -lm lerArquivos.c
mv a.out matrizSerial

./matrizSerial 10 10 10a 10 10 10b

##########################################################

# gera as matrizes 100x100

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# multiplica as matrizes 100x100

./matrizSerial 100 100 100a 100 100 100b

##########################################################

# gera as matrizes 1000x1000

./geraMatriz #gera matriz A

./geraMatriz #gera matriz B

##########################################################

# multiplica as matrizes 1000x1000 

./matrizSerial 1000 1000 1000a 1000 1000 1000b

##########################################################
