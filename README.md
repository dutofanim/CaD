# CaD
Trabalhos da matéria de Computação de Alto Desempenho

Laboratório de Programação PThreads

Exercício 1 – Crie um programa serial e um programa paralelo com PThreads que calculem a multiplicação de duas matrizes cujos valores estarão em arquivos anexos. Ambos os programas devem aceitar matrizes com tamanhos (número de linhas e colunas) a serem informados pelos usuários.
Prepare-se para gerar gráficos para cada um dos itens a seguir (instrumentar o código para gerar métricas necessárias).

a. Calcule o tempo de execução do programa serial para matrizes de tamanho 10 X 10; 100 X 100; 1000 X 1000
b. Calcule o tempo de execução do programa paralelo para matrizes de tamanho 10 X 10; 100 X 100; 1000 X 1000, com 2, 6, 4 e 8 threads
c. Responda as perguntas a seguir no relatório:
    • Há necessidade de sincronização entre os threads para resolver a multiplicação?
    • Qual foi o speedup em relação ao programa serial em cada uma das execuções?
    • Houve algum caso em que não houve speedup em relação ao programa serial? Se houve, qual a razão para isso?
    
    
Laboratório de Programação OpenMP

Exercício 2 - Para este laboratório, você deverá produzir um relatório (arquivo no formato PDF) com o resultado dos exercícios de programação propostos a seguir.
Junto com o relatório, você deve incluir os códigos fontes que você criou, além das instruções para compilalos (se puder providenciar um makefile, melhor) e executá-los.
Todos os arquivos devem estar compactados em um só arquivo (tar.gz ou zip) e devem ser publicados no ambiente virtual de aprendizagem (Moodle).
Exercício – Crie um programa serial e um programa paralelo com OpenMP que calcule a operação matricial D = A * B + C, onde todas as matrizes são n x n.
Prepare-se para gerar gráficos para cada um dos itens a seguir.
a. Calcule o tempo de execução do programa serial para matrizes de tamanhos 10^2 X 10^2; 10^3 X 10^3; 10^4 X 10^4.

b. Calcule o tempo de execução do programa paralelo para matrizes de tamanho 10^2 X 10^2;10^3 X 10^3; 10^4 X 10^4 com 2, 4, 8, 16 e 32 threads.

c.Responda as perguntas a seguir no relatório:

 - Há necessidade de sincronização entre os threads para resolver a multiplicação?

 - Qual foi o speedup em relação ao programa serial em cada uma das execuções?

 - Houve algum caso em que não houve speedup em relação ao programa serial? Se houve, qual a razão para isso?
