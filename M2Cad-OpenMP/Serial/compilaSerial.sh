###############  Gera o executável do gerador de Matriz ###############

gcc geraMatriz.c -o geraMatriz

#######################################################################

################  Gera o executável do programa Serial ################

gcc multMatSerial.c -lm -m64 lerArquivos.c -o multMatSerial

#######################################################################

##########################  Matrizes 10^2x10^2 ########################

./geraMatriz ##gera matriz 100a 100x100
./geraMatriz ##gera matriz 100b 100x100
./geraMatriz ##gera matriz 100c 100x100

#####  Executa o programa Serial com matriz 10^2x10^2 cinco vezes #####

./multMatSerial 100 100 100a 100 100 100b 100 100 100c
./multMatSerial 100 100 100a 100 100 100b 100 100 100c
./multMatSerial 100 100 100a 100 100 100b 100 100 100c
./multMatSerial 100 100 100a 100 100 100b 100 100 100c
./multMatSerial 100 100 100a 100 100 100b 100 100 100c

#######################################################################

##########################  Matrizes 10^3x10^3 ########################

./geraMatriz ##gera matriz 1000a 1000x1000
./geraMatriz ##gera matriz 1000b 1000x1000
./geraMatriz ##gera matriz 1000c 1000x1000

#####  Executa o programa Serial com matriz 10^3x10^3 cinco vezes #####

./multMatSerial 1000 1000 1000a 1000 1000 1000b 1000 1000 1000c
./multMatSerial 1000 1000 1000a 1000 1000 1000b 1000 1000 1000c
./multMatSerial 1000 1000 1000a 1000 1000 1000b 1000 1000 1000c
./multMatSerial 1000 1000 1000a 1000 1000 1000b 1000 1000 1000c
./multMatSerial 1000 1000 1000a 1000 1000 1000b 1000 1000 1000c

#######################################################################

##########################  Matrizes 10^4x10^4 ########################

./geraMatriz ##gera matriz 10000a 10000x10000
./geraMatriz ##gera matriz 10000b 10000x10000
./geraMatriz ##gera matriz 10000c 10000x10000

#####  Executa o programa Serial com matriz 10^4x10^4 cinco vezes #####

./multMatSerial 10000 10000 10000a 10000 10000 10000b 10000 10000 10000c
./multMatSerial 10000 10000 10000a 10000 10000 10000b 10000 10000 10000c
./multMatSerial 10000 10000 10000a 10000 10000 10000b 10000 10000 10000c
./multMatSerial 10000 10000 10000a 10000 10000 10000b 10000 10000 10000c
./multMatSerial 10000 10000 10000a 10000 10000 10000b 10000 10000 10000c

#######################################################################
