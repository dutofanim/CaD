#include <omp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "lerArquivos.h"
#include "multMatSerial.h"


char *fileName_mat1, *fileName_mat2, *fileName_mat3;
long int *mat1, *mat2, *mat3, *matFinal;
struct timeval ini_ler_mtz1, end_ler_mtz1, ini_ler_mtz2, end_ler_mtz2, ini_ler_mtz3, end_ler_mtz3, ini_mult, end_mult, tv;
unsigned int l_mat1, c_mat1, l_mat2, c_mat2, l_mat3, c_mat3, nThr;

struct timeval marcaTempo(struct timeval tv, time_t currenttime, char buffer[30]){
  gettimeofday(&tv, NULL); 
  currenttime=tv.tv_sec;
  strftime(buffer,30,"%d-%m-%Y %T.",localtime(&currenttime)); 
  printf("Time: %s%ld;\n\t\t", buffer, tv.tv_usec);
  return tv; 
}
void calculaTempo(struct timeval starttime, struct timeval entime){
  printf("Tempo total da execução: %ld us.\n\t\t", (entime.tv_sec * 1000000 + entime.tv_usec)-(starttime.tv_sec * 1000000 + starttime.tv_usec));
}

        long int *mMat(
                        unsigned int linMat1, unsigned int colMat1, long int *mat1, 
                        unsigned int linMat2, unsigned int colMat2, long int *mat2,                
                        unsigned int linMat3, unsigned int colMat3, long int *mat3,
                                                                long int *matFinal ){
                int i=0;
                int j=0;
                int x=0;
        
                for (i=0; i<linMat1; i++){
                for( j=0; j<colMat2; j++){
                                for ( x=0; x<colMat1; x++){
                                                matFinal[i*colMat2 + j] += mat1[i*colMat1+x] * mat2[x*colMat2+j];
                                        }
                                        matFinal[i*colMat2 + j] += mat3[i*colMat2 + j];
                                }
                }
                
                return matFinal; 
                }

int main (int argc, char **argv){

        char buffer[30];
        time_t curtime;

        l_mat1                  = atoi( argv[1] );
        c_mat1                  = atoi( argv[2] );      
        fileName_mat1   =               argv[3]  ;
        l_mat2                  = atoi( argv[4] );
        c_mat2                  = atoi( argv[5] );
        fileName_mat2   =               argv[6]  ;
        l_mat3                  = atoi( argv[7] );
        c_mat3                  = atoi( argv[8] );
        fileName_mat3   =               argv[9]  ;
         

        matFinal = (long int *) malloc(sizeof(long int) * l_mat1 * c_mat2); 
        if (matFinal == NULL){
                perror("I cannot allocate memory\n");
                exit(EXIT_FAILURE);
                return (long int)NULL;
        }

        for (int i=0; i<l_mat1;i++){
                for(int j=0;j<c_mat2;j++){
                        matFinal[i*c_mat2 + j] =0;          
                }
        }

    
        //Tempo de leitura do arquivo da matriz A
        printf("Operações com matrizes usando programa Serial.\n\t\tLendo matriz 'A': %d x %d;\n\t\t",l_mat1,c_mat1);
        ini_ler_mtz1    = marcaTempo(ini_ler_mtz1, curtime, buffer); 
        mat1                    = readFile(fileName_mat1, l_mat1, c_mat1);
        end_ler_mtz1    = marcaTempo(end_ler_mtz1, curtime, buffer);  
        calculaTempo(ini_ler_mtz1, end_ler_mtz1);

        //Tempo de leitura do arquivo da matriz B
        printf("Lendo matriz 'B': %d x %d;\n\t\t",l_mat2,c_mat2);
        ini_ler_mtz2    = marcaTempo(ini_ler_mtz2, curtime, buffer); 
        mat2                    = readFile(fileName_mat2, l_mat2, c_mat2);
        end_ler_mtz2    = marcaTempo(end_ler_mtz2, curtime, buffer); 
        calculaTempo(ini_ler_mtz2, end_ler_mtz2);

        //Tempo de leitura do arquivo da matriz C
        printf("Lendo matriz 'C': %d x %d;\n\t\t",l_mat3,c_mat3);
        ini_ler_mtz3    = marcaTempo(ini_ler_mtz3, curtime, buffer); 
        mat3                    = readFile(fileName_mat3, l_mat3, c_mat3);
        end_ler_mtz3    = marcaTempo(end_ler_mtz3, curtime, buffer); 
        calculaTempo(ini_ler_mtz3, end_ler_mtz3);

        printf("Iniciando cálculo das matrizes;\n\t\t");
        ini_mult                = marcaTempo(ini_mult, curtime, buffer); 
        matFinal                = mMat(l_mat1, c_mat1, mat1, l_mat2, c_mat2, mat2, l_mat3, c_mat3, mat3, matFinal);
        end_mult                = marcaTempo(end_mult, curtime, buffer); 
        calculaTempo(ini_mult, end_mult);

        printf("\n");

        free(mat1);
        free(mat2);
        free(mat3);        
        free(matFinal);
        return 0;
}
