/*
 * Laboratorio de Programacao PThreads
 *
 *    Ferramenta para geracao de arquivos com matrizes
 *
 * Autor: Giulliano Paes Carnielli (974153)
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/* Procedimento: imprime_matriz
 * Descricao: imprime uma matriz inteira quadrada, formatando a saida de acordo com parametros
 * Parametros:
 *    1. char nomeMat[32]: nome da matriz a ser impressa. Tamanho maximo = 31 caracteres + '\0'
 *    2. int matriz[][]: matriz que deve ser impressa
 *    3. int digits: numero de digitos esperado para a entrada (usado na formatacao)
 */
void imprime_matriz(char nomeMat[32], const int lin, const int col, int **matriz, int digits)
{
    char formato[8];
    char col_elipsis[5];
    char lin_elipsis[6];
    FILE *fpt = fopen(nomeMat, "w");

    // Ajusta precisao para formatacao da saida
    sprintf(formato, "%%.%dd  ", digits);

    // Se a matriz tiver tamanho maior que 15 x 15, ent�o a visualizacao sera
    // parcial (em steps)

    // Imprime matriz
    int i, j;

    // Imprime nome da matriz
    //printf("\n%s\n", nomeMat);
    for (i = 0; i < lin; i++)
    {
        //printf("\t"); // Tabulacao de inicio de linha
        for (j = 0; j < col; j++)
        {
            // Formata cada entrada com dois digitos, preenchidos com zeros
            //printf(formato, matriz[i][j]);
            fprintf(fpt, formato, matriz[i][j]);
        }
        //printf("\n");
        fprintf(fpt, "\n");
    }
    fclose(fpt);
}

/* Procedimento: cria_matriz
 * Descricao: cria uma matriz inteira quadrada, de acordo com parametros
 * Parametros:
 *    1. nlin: numero de linhas
 *    2. ncol: numero de colunas
 *    3. valor maximo: valor maximo a ser inserido
 * Retorno: referencia para matriz criada
 */
int** cria_matriz(const int nlin, const int ncol, int max_val)
{
    int **mat; // matriz de elementos do tipo int

    mat = (int **)calloc(nlin,sizeof(int *));
    // aloca vetor de ponteiros para variaveis int (linhas)
    if (mat != NULL)
    {
        int i, j;
        for (i=0; i < nlin; i++)
        {
            // Aloca vetor de inteiros (colunas)
            mat[i] = (int *)calloc(ncol, sizeof(int));
            for (j = 0; max_val > 0 && j < ncol; j++)
            {
                mat[i][j] = rand() % (max_val + 1);
            }

        }
    }
    return mat;
}

int main(int argc, char *argv[])
{
    // Inicializa o gerador de numeros aleatorios
    srand(time(0));
    int lin, col, max_val = -1;
    char filename[31];

    printf("Informe nome da matriz: (30 caracteres)\n");
    scanf(" %s", filename);

    printf("Informe dimensoes da matriz: lin col\n");
    scanf(" %i %i", &lin, &col);

    do
    {
        printf("Informe valor maximo dos elementos da matriz: (0 - 100000)\n");
        scanf(" %i", &max_val);
    }
    while(max_val < 0 || max_val > 100000);

    int **matA = cria_matriz(lin, col, max_val);

    // Imprime matrizes A e B
    imprime_matriz(filename, lin, col, matA, 1);
    return 0;
}

